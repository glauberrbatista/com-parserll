/*****************************************************************
 * Analisador Sintatico LL(1)                                     *
 * Exemplo p/ Disciplina de Compiladores                          *
 * Cristiano Damiani Vasconcellos                                 *
 ******************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <assert.h>

/* Nao terminais o bit mais significativo ligado indica que se trata de um nao
   terminal */

#define E       0x8001 //E
#define EL      0x8002 //E'
#define T       0x8003 //T
#define TL      0x8004 //T'
#define A       0x8005 //A
#define AL      0x8006 //A'
#define F       0x8007 //F

/* Terminais */
#define ERRO    0x0000 /* erro               */
#define AND     0x0100 /* e                  */
#define OR      0X0200 /* ou                 */
#define IMP     0x0300 /* implicacao ->      */
#define BIMP    0x0400 /* bi inplicação <->  */
#define NOT     0x0500 /* negacao            */
#define CONST   0x0600 /* constante          */
#define APAR    0x0700 /* abre parenteses    */
#define FPAR    0x0800 /* fecha parenteses   */
#define FIM     0x0900 /* end of input       */

//Mascaras
#define NTER   0x8000
#define NNTER  0x7FFF

#define TAMPILHA 100

struct Pilha {
    int topo;
    int dado[TAMPILHA];
};

/* Producoes a primeira posicao do vetor indica quantos simbolos
   gramaticais a producao possui em seu lado direito */

const int PROD1[]   = {2, T, EL};                       // E  => TE'
const int PROD2[]   = {3, BIMP, T, EL};                 // E' => <->TE'
const int PROD3[]   = {0};                              // E' => vazio
const int PROD4[]   = {2, A, TL};                       // T  => AT'
const int PROD5[]   = {3, IMP, A, TL};                  // T' => ->AT'
const int PROD6[]   = {0};                              // T' => vazio
const int PROD7[]   = {2, F, AL};                       // A  => FA'
const int PROD8[]   = {3, AND, F, AL};                  // A' => &FA'
const int PROD9[]   = {3, OR, F, AL};                   // A' => |FA'
const int PROD10[]  = {0};                              // A' => vazio
const int PROD11[]  = {1, CONST};                       // F  => c
const int PROD12[]  = {3, APAR, E, FPAR};               // F  => (E)
const int PROD13[]  = {2, NOT, F};                      // F  => ~F


// vetor utilizado para mapear um numero e uma producao.
const int *PROD[] = {NULL, PROD1, PROD2, PROD3, PROD4, PROD5, PROD6, PROD7, PROD8, PROD9, PROD10, PROD11, PROD12, PROD13};

// Tabela sintatica LL(1). Os numeros correspondem as producoes acima.
const int STAB[7][9] = {{0,  0,  0,  0,  1,  1,  1,  0,  0},
                        {0,  0,  0,  2,  0,  0,  0,  3,  3},
                        {0,  0,  0,  0,  4,  4,  4,  0,  0},
                        {0,  0,  5,  6,  0,  0,  0,  6,  6},
                        {0,  0,  0,  0,  7,  7,  7,  0,  0},
                        {8,  9, 10, 10,  0,  0,  0, 10, 10},
                        {0,  0,  0,  0, 13, 11, 12,  0,  0}};

/*****************************************************************
 * int lex (char *str, int *pos)                                  *
 * procura o proximo token dentro de str a partir de *pos,incre-  *
 * menta o valor de *pos a medida que faz alguma transicao de     *
 * estados.                                                       *
 * Retorna o inteiro que identifica o token encontrado.           *
 ******************************************************************/

int lex (char *str, int *pos)
{
    int estado = 0;
    char c;

    while (1)
    {
        c =  str[*pos];

        switch(estado)
        {
            case 0:
                switch (c)
                {
                    case ' ':
                        (*pos)++;
                        break;
                    case 'F':
                        (*pos)++;
                        return CONST;
                    case 'V':
                        (*pos)++;
                        return CONST;
                    case '-': //usado no implica ->
                        (*pos)++;
                        estado = 1;
                        break;
                    case '<': //usado no bi implica <->
                        (*pos)++;
                        estado = 2;
                        break;
                    case '&':
                        (*pos)++;
                        return AND;
                    case '|':
                        (*pos)++;
                        return OR;
                    case '(':
                        (*pos)++;
                        return APAR;
                    case ')':
                        (*pos)++;
                        return FPAR;
                    case '~':
                        (*pos)++;
                        return NOT;
                    case '\0':
                        return FIM;
                    default:
                        (*pos)++;
                        return ERRO;
                }
                break;
            case 1:
                if( c == '>'){
                    (*pos)++;
                    estado = 0;
                    return IMP;
                } else {
                    return ERRO;
                }
                break;
            case 2:
                if(c == '-'){
                    (*pos)++;
                    estado = 3;
                } else {
                    return ERRO;
                }
                break;
            case 3:
                if(c == '>'){
                    (*pos)++;
                    return BIMP;
                } else {
                    return ERRO;
                }
                break;
            default:
                printf("Lex:Estado indefinido");
                exit(1);
        }
    }
}

/*****************************************************************
 * void erro (char *erro, char *expr, int pos)                    *
 * imprime a mensagem apontado por erro, a expressao apontada por *
 * expr, e uma indicacao de que o erro ocorreu na posicao pos de  *
 * expr. Encerra a execucao do programa.                          *
 ******************************************************************/

void erro (char *erro, char *expr, int pos)
{
    int i;
    printf("%s", erro);
    printf("\n%s\n", expr);
    for (i = 0; i < pos-1; i++)
        putchar(' ');
    putchar('^');
    exit(1);
}

/*****************************************************************
 * void inicializa(struct Pilha *p)                               *
 * inicializa o topo da pilha em -1, valor que indica que a pilha *
 * esta vazia.                                                    *
 ******************************************************************/

void inicializa(struct Pilha *p)
{
    p->topo = -1;
}

/*****************************************************************
 * void insere (struct Pilha *p, int elemento                     *
 * Insere o valor de elemento no topo da pilha apontada por p.    *
 ******************************************************************/

void insere (struct Pilha *p, int elemento)
{
    if (p->topo < TAMPILHA)
    {
        p->topo++;
        p->dado[p->topo] = elemento;
    }
    else
    {
        printf("estouro de pilha");
        exit (1);
    }
}

/*****************************************************************
 * int remover (struct Pilha *p)                                  *
 * Remove e retorna o valor armazenado no topo da pilha apontada  *
 * por p                                                          *
 ******************************************************************/

int remover (struct Pilha *p)
{
    int aux;

    if (p->topo >= 0)
    {
        aux = p->dado[p->topo];
        p->topo--;
        return aux;
    }
    else
    {
        printf("Pilha vazia");
        exit(1);
    }
    return 0;
}

/*****************************************************************
 * int consulta (struct Pilha *p)                                 *
 * Retorna o valor armazenado no topo da pilha apontada por p     *
 ******************************************************************/


int consulta (struct Pilha *p)
{
    if (p->topo >= 0)
        return p->dado[p->topo];
    printf("Pilha vazia");
    exit(1);
}

/*****************************************************************
 * void parser (char *expr)                                       *
 * Verifica se a string apontada por expr esta sintaticamente     *
 * correta.                                                       *
 * Variaveis Globais Consultadas: STAB e PROD                     *
 ******************************************************************/


void parser(char *expr)
{
    struct Pilha pilha;
    int x, a, nProd, i, *producao;
    int pos = 0;

    inicializa(&pilha);
    insere(&pilha, FIM);
    insere(&pilha, E);

    if ((a = lex(expr, &pos)) == ERRO)
        erro("Erro lexico", expr, pos);
    do
    {

        x = consulta(&pilha);
        if (!(x&NTER))
        {
            if (x == a)
            {
                remover (&pilha);
                if ((a = lex(expr, &pos)) == ERRO)
                    erro("Erro lexico", expr, pos);
            }
            else
                erro("Erro sintatico",expr, pos);
        }
        if (x&NTER)
        {
            nProd = STAB[(x&NNTER)-1][(a>>8) - 1];
            if (nProd)
            {
                remover (&pilha);
                producao = PROD[nProd];
                for (i = producao[0]; i > 0; i--)
                    insere (&pilha, producao[i]);
            }
            else{
                erro ("Erro sintatico", expr, pos);
            }
        }
    } while (x != FIM);
}

int main()
{
    char expr[100];

    printf("\nDigite uma expressao: ");
    gets(expr);
    parser(expr);
    printf("Expressao sintaticamente correta\n");


    return EXIT_SUCCESS;
}

